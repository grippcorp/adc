import React, { Component } from 'react';
import { GoMail } from 'react-icons/go';
import { FaWhatsapp } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { Container, Title, Nav } from './styles';

import Social from '../SocialMedia/index';

export default class NavMain extends Component {
  render() {
    return (
      <Container>
        <Title>
          <Title>
            <GoMail size={35} /> aprendadireitocompleto@gmail.com
          </Title>
          <Title id="whats">
            <FaWhatsapp size={28} /> (13)99756.9515
          </Title>
        </Title>
        <Social />
        <Nav>
          <button type="button">Login</button>
          <Link to="register">Registrar</Link>
          <button type="button" id="assinar">
            QUERO ASSINAR
          </button>
        </Nav>
      </Container>
    );
  }
}
