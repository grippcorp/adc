import styled from 'styled-components';

export const Container = styled.div`
  display: flex;

  justify-content: center;
  margin-bottom: 3%;
`;

export const Text = styled.div``;

export const WrapperContainer = styled.div`
  background: #1b1b1b;
  height: 400px;
`;
